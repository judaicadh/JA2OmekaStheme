Condorcet (theme for Omeka S)
=============================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

Voir le [Lisez-moi] en français

[Condorcet] is a theme for [Omeka S] designed for the site [bibnum.campus-condorcet.fr].

**WARNING**: The logos cannot be reused without express permissions.


Installation
------------

See general end user documentation for [Installing a module].

Uncompress files and rename the theme folder as you want, preferably without
spaces and special characters.

This theme works better with these associated modules:
- Module [Advanced Search] to manage the search.
- Module [Block Plus] to manage the site pages.
- Module [Iiif Server] to manage viewer.
- [Mirador], [Universal Viewer], and [Image Server] for the item page.
- Module [Reference] to display facets in the results with the internal adapter.
  It is not needed for external search engines that can manage facets natively.
  It should be at least version 3.4.16 or last one when possible.

The current version of the theme is not designed to build exhibit or static
content, so you may have to update the theme for missing blocks.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [theme issues] page on GitLab.


License
-------

### Theme (without logos)

This theme, **except the logos**, is published under the [CeCILL v2.1] license,
compatible with [GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.

### Assets

The logos are not open sourced and are under copyright.

### Libraries

See directory asset/vendor for related copyrigths and licenses.


Copyright
---------

* Copyright Cathy Drévillon, Véronica Holguin, Denis Chiron, Daniel Berthereau for [Sempiternelia], 2021-2024


[Condorcet]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet
[Lisez-moi]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet/-/blob/master/LISEZMOI.md
[bibnum.campus-condorcet.fr]: https://bibnum.campus-condorcet.fr
[Omeka S]: https://omeka.org/s
[Installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[Advanced Search]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedSearch
[Block Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-BlockPlus
[Iiif Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-IiifServer
[Mirador]: https://gitlab.com/Daniel-KM/Omeka-S-module-Mirador
[Universal Viewer]: https://gitlab.com/Daniel-KM/Omeka-S-module-UniversalViewer
[Image Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-ImageServer
[Reference]: https://gitlab.com/Daniel-KM/Omeka-S-module-Reference
[theme issues]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[Sempiternelia]: https://sempiternelia.net
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
